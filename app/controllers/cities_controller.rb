class CitiesController < ApplicationController
  before_action :set_city, only: [:show, :edit, :update, :destroy]

  def index
    @cities = City.all.paginate(page: params[:page], per_page: 5)
  end

  def show
    @city = City.find(params[:id])
  end

  def new
    @city = City.new
  end

  def search

  end

  def edit

  end

  def details

  end

  def create
    @city = City.new(city_params)
    respond_to do |format|
      if @city.save
        format.html { redirect_to @city, notice: 'Poprawnie utworzony.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @city.update(city_params)
        format.html { redirect_to @city, notice: 'Zaktualizowano poprawnie.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @city.destroy
    respond_to do |format|
      format.html { redirect_to '/cities', notice: 'Wpis został usunięty.' }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_city
    @city = City.find(params[:id])
  end

  # Never trust parameters from internet :-)
  def city_params
    params.require(:city).permit(:name, :short_name)
  end

end
