Rails.application.routes.draw do
  root to: 'visitors#index'
  devise_for :users
  resources :users
  get '/all_users/:id', :controller=>'users', :action=>'show'

  resources :cities do
    get '/cities/', :controller=>'cities', :action=>'index'
    get '/cities/:id', :controller=>'cities', :action=>'show'
    get '/cities/:id/edit', :controller=>'cities', :action=>'edit'
    get '/cities/new', :controller=>'cities', :action=>'create'
  end

end
